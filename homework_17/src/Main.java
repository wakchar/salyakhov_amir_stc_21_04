import pattern.observer.Editor;
import pattern.observer.EmailNotificationListener;
import pattern.observer.LogOpenListener;

public class Main {

    public static void main(String[] args) {
        Editor editor = new Editor();
        editor.events.subscribe("open", new LogOpenListener("src/file.txt"));
        editor.events.subscribe("save", new EmailNotificationListener("Wakchar@gmail.com"));

        try {
            editor.openFile("test.txt");
            editor.saveFile();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
