package models;

import java.util.Objects;
import java.util.Random;

public class Person {
    private final String[] HUMAN_GENDERS = {"MAN", "WOMAN"};
    private final String[] MAN_NAMES = {"Дмитрий", "Максим", "Даниил", "Кирилл", "Ярослав", "Денис", "Никита", "Иван"};
    private final String[] WOMAN_NAMES = {"Анастасия", "Анна", "Мария", "Дарья", "Алина", "Ирина", "Екатерина", "Арина"};
    private int age;
    private String name;
    private String gender;

    public Person(int age, String gender, String name) {
        this.age = age;
        this.gender = gender;
        this.name = name;
    }

    public Person() {
    }

    private int setAgeToPerson() {
        Random random = new Random();
        age = random.nextInt(100);
        return age;

    }

    private String setPersonGender() {
        Random random = new Random();
        String selectedGender = HUMAN_GENDERS[random.nextInt(HUMAN_GENDERS.length)];
        selectedNameByGender(selectedGender);
        gender = selectedGender;
        return gender;
    }

    private void selectedNameByGender(String selectedGender) {
        Random random = new Random();
        if (selectedGender.equals("MAN")) {
            name = MAN_NAMES[random.nextInt(MAN_NAMES.length)];
         } else name = WOMAN_NAMES[random.nextInt(WOMAN_NAMES.length)];
    }

    private String returnSelectedNameByGender() {
        return name;
    }

    public Person createAndGetPerson() {
         return new Person(setAgeToPerson(), setPersonGender(), returnSelectedNameByGender());
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age && name.equals(person.name) && gender.equals(person.gender);
    }

    @Override
    public int hashCode() {
        return Objects.hash(age, name, gender);
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }
}
