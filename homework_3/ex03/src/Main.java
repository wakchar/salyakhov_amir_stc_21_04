import models.Person;
import service.PersonArrayGenerator;
import service.Quicksort;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int length = 101;
        Person[] pers = new Person[length];
        Person[] tmpArrayForSortedPeople;
        PersonArrayGenerator generator = new PersonArrayGenerator();
        tmpArrayForSortedPeople = generator.generateAndFillPersonArray(pers);
        Quicksort quicksort = new Quicksort();
        quicksort.sortedByAgePerson(tmpArrayForSortedPeople);
        System.out.println(Arrays.toString(tmpArrayForSortedPeople));
    }
}
