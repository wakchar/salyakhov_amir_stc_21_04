package service;

import models.Person;

public class PersonArrayGenerator {

    public Person[] generateAndFillPersonArray(Person[] arrayPerson) {
        int length = arrayPerson.length;
        Person person = new Person();

        for (int i = 0; i < length; ) {
            Person tmpPerson = person.createAndGetPerson();
            int putAddress = findAvailabilityOrReturnNotAvailableValue(tmpPerson, arrayPerson);
            if (putAddress != -1) {
                arrayPerson[putAddress] = tmpPerson;
                i++;
            }
        }
        return arrayPerson;
    }

    private int findAvailabilityOrReturnNotAvailableValue(Person putPerson, Person[] arrayPerson) {
        int availabilityPutAddress = -1;
        int personAge = putPerson.getAge();
        int placeToPut = personAge % 10;
        while (placeToPut <= arrayPerson.length - 1) {
            if (arrayPerson[placeToPut] == null) {
                return placeToPut;
            } else placeToPut += 10;
        }
        return availabilityPutAddress;
    }
}
