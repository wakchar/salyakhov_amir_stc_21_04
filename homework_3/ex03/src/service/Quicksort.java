package service;

import models.Person;

public class Quicksort {

    private static void quickSortRecursive(Person[] toSortPersonArray, int lower, int higher) {
        if (higher <= lower) return;
        int pivot = partitionLomuto(toSortPersonArray, lower, higher);
        quickSortRecursive(toSortPersonArray, lower, pivot - 1);
        quickSortRecursive(toSortPersonArray, pivot + 1, higher);
    }

    @SuppressWarnings("unchecked")
    private static int partitionLomuto(Person[] array, int lower, int higher) {
        int counter = lower;

        for (int i = lower; i < higher - 1; i++) {
            if (array[i].getAge() > array[higher - 1].getAge()) {
                Person temp = array[counter];
                array[counter] = array[i];
                array[i] = temp;
                counter++;
            }
        }
        Person temp = array[higher - 1];
        array[higher - 1] = array[counter];
        array[counter] = temp;

        return counter + 1;
    }

    public void sortedByAgePerson(Person[] toSortPersonArray) {
        Person[] sortedByAgePerson = new Person[toSortPersonArray.length];
        quickSortRecursive(toSortPersonArray, 0, toSortPersonArray.length - 1);
    }
}
