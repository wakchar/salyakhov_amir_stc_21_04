import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            try {
                int countNumbers = scanner.nextInt();
                if (countNumbers <= 0) {
                    throw new Exception("Пожалуйста введите число больше 0!");
                } else findSqrtForNumbers(generatedRandomNumbers(countNumbers));
                break;
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    public static int[] generatedRandomNumbers(int countNumbers) {
        int[] randomNumbers = new int[countNumbers];
        for (int i = 0; i < countNumbers; i++) {
            Random random = new Random();
            randomNumbers[i] = random.nextInt();
        }
        return randomNumbers;
    }

    public static void findSqrtForNumbers(int[] generatedRandomNumbers) {
        double q;
        int checkedQ;
        int power = 2;

        for (int generatedRandomNumber : generatedRandomNumbers) {
            q = Math.sqrt(generatedRandomNumber);
            checkedQ = (int) Math.pow(q, power);
            if (checkedQ == generatedRandomNumber) {
                System.out.print(generatedRandomNumber + "  ");
                System.out.println(checkedQ);
            }
        }
    }
}
