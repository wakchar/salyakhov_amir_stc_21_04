import customExeptions.CustomExemptions;

public class Main {

    public static void main(String[] args) {

        String[] outMessage = {"Hello", "world", "!"};
        Object nullObject = null;
        int a = 50;
        int b = 10;

        nullPointerException(nullObject);
        arrayIndexOutOfBoundsException(outMessage);
        someSeveralError(a, b);
    }

    private static void nullPointerException(Object x) {
        try {
            System.out.println(x.getClass());
        } catch (NullPointerException e) {
            System.err.println("О нет, " + e + " !");
        }
    }

    private static void arrayIndexOutOfBoundsException(String[] outMessage) {
        try {
            System.out.println(outMessage[4]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("О нет! " + e);
        }
    }

    private static void someSeveralError(int a, int b) {
        try {
            if (a > b)
                throw new CustomExemptions();
        } catch (CustomExemptions e) {
            System.err.println("Ошибка! " + e);
        }
    }
}
