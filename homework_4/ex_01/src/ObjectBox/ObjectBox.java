package ObjectBox;

import java.util.HashSet;
import java.util.Set;

public class ObjectBox<T> {

    private final Set<T> objectHashSet = new HashSet<>();

    public ObjectBox() {
    }

    public void addObject(T putToBox){
        objectHashSet.add(putToBox);
    }

    public void deleteObject(T deleteObject){
        objectHashSet.remove(deleteObject);
    }

    public void dump(){
       for(T strToPrint : objectHashSet){
           System.out.print(strToPrint + " ");
       }
    }

    @Override
    public String toString() {
        return "ObjectBox{" +
                "objectHashSet=" + objectHashSet +
                '}';
    }

    public Set<T> getObjectHashSet() {
        return objectHashSet;
    }
}
