package MathBox;

import ObjectBox.ObjectBox;

import java.util.stream.IntStream;


public class MathBox<E extends Number> extends ObjectBox<E> {

    public MathBox(E[] array) {
        for (E number : array) {
            addObject(number);
        }
    }

    public long summator() {
        return getObjectHashSet().stream().mapToInt(a -> a.intValue()).sum();
    }

    public int[] splitter(int div) {
        return getObjectHashSet().stream()
                .flatMapToInt(a -> IntStream.of(a.intValue() / div))
                .toArray();

    }
}
