import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String nickName;
        System.out.println("Введите свой никнейм");
        Scanner nickScanner = new Scanner(System.in);
        nickName = nickScanner.nextLine();
        SocketClient client = new SocketClient("localhost", 10501);
        sendMessege(nickName, client);
    }

    public static void sendMessege(String nickName, SocketClient client){
        Scanner scanner = new Scanner(System.in);
        client.sendMessage("#" + nickName);
        String message = scanner.nextLine();
        while (!message.equals("exit")) {
            client.sendMessage(nickName + ": "+ message);
            message = scanner.nextLine();
        }
    }
}
