package server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class EchoServer {

    public static List<Socket> userSocketAddress = new ArrayList<>();
    public static Map<String, Socket> userMap = new HashMap<>();

    Socket userSocket = null;

    public void startServer(int port) {
        try {
            ServerSocket echoServerSocket = new ServerSocket(port);
            while (true) {
                userSocket = echoServerSocket.accept();
                userHandler user = new userHandler(userSocket);
                userSocketAddress.add(user.clientSocket);
                new Thread(user).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void sendMessageToAllUsers(String message) throws IOException {
        for (Socket socket : userSocketAddress) {
            PrintWriter send = new PrintWriter(socket.getOutputStream(), true);
            send.println(message);
        }
    }

    public static void sendMessageToSelectedUser(String message, String userNickName) throws IOException {
        Socket socket = userMap.get(userNickName);
        System.out.println("socket" + socket);
        PrintWriter send = new PrintWriter(socket.getOutputStream(), true);
        send.println(message);
    }

}

