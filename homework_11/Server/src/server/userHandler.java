package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class userHandler implements Runnable {

    public Socket clientSocket;
    public String nickName;
    private BufferedReader fromClient;
    private PrintWriter toClient;

    public userHandler(Socket clientSocket) {
        this.clientSocket = clientSocket;
        try {
            this.fromClient = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            this.toClient = new PrintWriter(clientSocket.getOutputStream(), true);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                String messages;
                if ((messages = fromClient.readLine()) != null) {
                    String[] filterMessage = messages.split(" ");
                    if (filterMessage[0].equals("#")) {
                        nickName = String.join(filterMessage[1]);
                    }
                    if (filterMessage.length > 1) {
                        if (filterMessage[1].equals("@")) {
                            String privateMessage = String.join(filterMessage[2]);
                            EchoServer.sendMessageToSelectedUser(messages, privateMessage);
                        } else
                            EchoServer.sendMessageToAllUsers(messages);
                    }
                    EchoServer.userMap.put(nickName, clientSocket);
                    System.out.println(clientSocket);
                    if (messages.equals("exit")) {
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.err.println("Handler error");
            }
        }
    }
}
