package services;

import server.User;

public interface ServerServices {
    User connectOrUpdateUser(User user);
    void disconnectUserFromServer();
}
