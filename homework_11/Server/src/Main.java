import server.EchoServer;

public class Main {

    public static void main(String[] args) {
        EchoServer startServer = new EchoServer();
        startServer.startServer(10501);
    }
}
