package ReadWritePackage;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;

public class FileReaderService {

    public static String readFromFileWithoutDoubles() {
        String tmp = readFromFile();
        return wordFilter(tmp);
    }

    private static String readFromFile() {
        StringBuilder sb = new StringBuilder();
        try {
            FileInputStream readFile = new FileInputStream(".//ex_01//src//inputdata.txt");
            int i;
            while (((i = readFile.read()) != -1)) {
                sb.append((char) i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private static String wordFilter(String wordsTofFilter) {
        return Arrays.stream(wordsTofFilter.split(" ")).distinct().collect(Collectors.joining(" "));
    }
}
