package ReadWritePackage;

import java.io.FileOutputStream;
import java.io.IOException;

public class FileWriterService {

    public static void dataWriter(String dataToWrite) {
        try {
            FileOutputStream outData = new FileOutputStream("C://java_inno//salyakhov_amir_stc_21_04//homework_7//ex_01//src//outputdata.txt");
            byte[] buffer = dataToWrite.getBytes();
            outData.write(buffer, 0, buffer.length);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
