/*
*Написать программу, читающую текстовый файл. Программа должна составлять отсортированный по алфавиту список слов,
* найденных в файле и сохранять его в файл-результат.
*Найденные слова не должны повторяться, регистр не должен учитываться.
*Одно слово в разных падежах – это разные слова.
*/

import ReadWritePackage.FileReaderService;
import ReadWritePackage.FileWriterService;

public class Main {

    public static void main(String[] args) {
        FileReaderService read = new FileReaderService();
        String data = read.readFromFileWithoutDoubles();
        String sortedWords = WordSortService.arrayFromString(data);
        FileWriterService.dataWriter(sortedWords);
    }
}
