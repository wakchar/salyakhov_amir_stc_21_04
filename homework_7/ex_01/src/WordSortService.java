import java.util.Arrays;

public class WordSortService {

    public static String arrayFromString(String inputData) {
        return quickSort(inputData.split(" "));
    }

    public static <V> String quickSort(Comparable<V>[] a) {
        quickSortRecursive(a, 0, a.length - 1);
        return Arrays.toString(a);
    }

    private static <V> void quickSortRecursive(Comparable<V>[] a, int lower, int higher) {
        if (higher <= lower) return;
        int j = partitionLomuto(a, lower, higher);
        quickSortRecursive(a, lower, j - 1);
        quickSortRecursive(a, j + 1, higher);
    }

    @SuppressWarnings("unchecked")
    private static <V> int partitionLomuto(Comparable<V>[] array, int lower, int higher) {
        int counter = lower;

        for (int i = lower; i < higher; i++) {
            if (0 > array[i].compareTo((V) array[higher])) {
                Comparable<V> temp = array[counter];
                array[counter] = array[i];
                array[i] = temp;
                counter++;
            }
        }
        Comparable<V> temp = array[higher];
        array[higher] = array[counter];
        array[counter] = temp;

        return counter;
    }
}
