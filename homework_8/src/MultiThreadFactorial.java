import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.IntStream;

public class MultiThreadFactorial {

    public static BigInteger[] multiThreadFactorial(int[] array, int countOfWorkThreads) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(countOfWorkThreads);
        BigInteger[] result = new BigInteger[array.length];
        Future[] future = new Future[array.length];
        long startTimer = ZonedDateTime.now().toInstant().toEpochMilli();
        Map<Integer, BigInteger> mapa = new ConcurrentHashMap<>();
        for (int i = 0; i < array.length; i++) {
            int j = i;
            int finalI = i;
            future[j] = executorService.submit(() -> {
                if (mapa.containsKey(array[finalI])) {
                    result[j] = mapa.get(array[finalI]);
                    return result[j];
                }
                if (array[j] < 2) {
                    result[j] = BigInteger.valueOf(1);
                } else {
                    result[j] = IntStream.rangeClosed(2, array[j]).mapToObj(BigInteger::valueOf).reduce(BigInteger::multiply).get();
                }
                mapa.put(array[finalI], result[j]);
                return result[j];
            });
        }
        for (Future bigIntegerFuture : future) {
            bigIntegerFuture.get();
        }
        long endTimer = ZonedDateTime.now().toInstant().toEpochMilli();
        System.out.println("Время выполнения: " + (endTimer - startTimer) + " миллисекунд." + " Stream метод");
        executorService.shutdown();
        return result;
    }
}
