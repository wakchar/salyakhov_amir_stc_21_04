import java.math.BigInteger;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;

/*
 *Дан массив случайных чисел.
 *Написать программу для вычисления факториалов всех элементов массива.
 *Использовать пул потоков для решения задачи.
 */

public class Main {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        int[] array = {10, 10, 5, 808, 5, 44, 5, 10, 10, 23, 808, 44, 5, -1};
        BigInteger[] result = MultiThreadFactorial.multiThreadFactorial(array, 2);
        System.out.println(Arrays.toString(result) + " Stream метод");
        BigInteger[] result2 = IterativeFunction.IterativeFunction(array, 2);
        System.out.println(Arrays.toString(result2) + " Iterative метод");
        int end = 0;
    }
}
