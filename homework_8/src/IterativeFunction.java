import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class IterativeFunction extends Thread {

    public static BigInteger[] IterativeFunction(int[] array, int countOfWorkThreads) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(countOfWorkThreads);
        BigInteger[] result = new BigInteger[array.length];
        Future[] future = new Future[array.length];
        long startTimer = ZonedDateTime.now().toInstant().toEpochMilli();
        for (int i = 0; i < array.length; i++) {
            int finalI = i;
            int f = finalI;
            future[f] = executorService.submit(() -> {
                BigInteger value = BigInteger.ONE;
                for (int j = 1; j <= array[finalI]; j++) {
                    if (array[finalI] <= 1) {
                        result[finalI] = BigInteger.valueOf(1);
                    } else {
                        value = value.multiply(BigInteger.valueOf(j));
                    }
                }
                result[finalI] = value;
            });
        }
        for (Future bigIntegerFuture : future) {
            bigIntegerFuture.get();
        }
        executorService.shutdown();
        long endTimer = ZonedDateTime.now().toInstant().toEpochMilli();
        System.out.println("Время выполнения: " + (endTimer - startTimer) + " миллисекунд." + " Iterative метод");
        return result;
    }

}
