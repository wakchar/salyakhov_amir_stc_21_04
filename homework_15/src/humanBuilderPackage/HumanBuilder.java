package humanBuilderPackage;

public class HumanBuilder {

    private final String firstName;
    private final String lastName;
    private final int age;

    private HumanBuilder(Builder builder) {
        firstName = builder.firstName;
        lastName = builder.lastName;
        age = builder.age;
    }

    @Override
    public String toString() {
        return "HumanBuilder{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }

    public static class Builder {  //Объявляю класс билдера

        private String firstName;
        private String lastName;
        private int age;

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder age(int age) {
            this.age = age;
            return this;
        }

        public HumanBuilder build() {
            return new HumanBuilder(this);
        }
    }
}
