import humanBuilderPackage.HumanBuilder;

public class Main {

    public static void main(String[] args) {

        HumanBuilder Amir = new HumanBuilder.Builder()
                .firstName("Amir")
                .lastName("Salyakhov")
                .age(26)
                .build();
        System.out.println(Amir);


        HumanBuilder SomeHuman = new HumanBuilder.Builder()
                .firstName("Name")
                .age(26)
                .build();
        System.out.println(SomeHuman);
    }
}
