package ru.paymentservice.payments.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import ru.paymentservice.payments.models.PaymentBilling;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.paymentservice.payments.models.PaymentResult;
import ru.paymentservice.payments.repositories.BillingRepository;

import java.util.List;

@RestController
@RequestMapping(value = "/gateway")
public class PaymentController {

    Logger logger = LoggerFactory.getLogger(PaymentController.class);

    @Autowired
    private BillingRepository paymentBilling;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/payments")
    public List<PaymentBilling> getAllPayments() {
        return paymentBilling.findAll();
    }

    @PostMapping(value = "/payment", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void registerPaymentBilling(@RequestBody PaymentBilling newPaymentBilling) {
        PaymentResult response = new PaymentResult();
        response.setCheckNumber(newPaymentBilling.getOrderId());
        response.setPaymentStatus("success");
        response.setAmount(newPaymentBilling.getPaymentSum());
        BillingResponse bill = new BillingResponse();
        bill.SendResponse(response);
        logger.info("data written");
        paymentBilling.save(newPaymentBilling);
    }
}
