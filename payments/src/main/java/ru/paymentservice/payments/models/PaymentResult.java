package ru.paymentservice.payments.models;

import lombok.*;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString

public class PaymentResult {

    private int checkNumber;
    private double amount;
    private String paymentStatus;
}

