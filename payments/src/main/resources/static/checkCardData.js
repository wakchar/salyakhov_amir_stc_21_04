function checkCardAvailability() {
    var carNumber = document.getElementById("CardNumber").value;
    var carNumberLength = carNumber.length;
    var firstName = document.getElementById("firstname").value;
    var lastname = document.getElementById("lastname").value;
    var expCard = document.getElementById("exp").value;
    var cvvCard = document.getElementById("cvv").value;

    if (carNumberLength === 16 && luhnCheck(carNumber) === true && firstName.length > 0
        && lastname.length > 0 && expCard > 0 && cvvCard>3) {
        document.getElementById("btn").disabled = false;
    }
}