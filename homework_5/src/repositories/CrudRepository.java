package repositories;

import models.Person;

import java.util.List;

public interface CrudRepository<T> {
    void addPetToList(T entity);

    List<T> findPetByPetName(String entity);

    void updatePetById(Integer value, String newPetName, Person newOwner, double petWeight);

    List<T> printAllPets();
}
