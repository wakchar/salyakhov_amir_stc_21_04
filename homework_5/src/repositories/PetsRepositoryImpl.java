package repositories;

import models.Person;
import models.PetsTemplate;

import java.util.*;

public class PetsRepositoryImpl implements PetsRepository {

    private final Map<Integer, PetsTemplate> petsMap = new TreeMap<>();

    private final Map<String, List<PetsTemplate>> indexMap = new TreeMap<>();


    @Override
    public void addPetToList(PetsTemplate entity) {
        List<PetsTemplate> pestList = indexMap.getOrDefault(entity.getPetName(), new LinkedList<>());
        pestList.add(entity);
        indexMap.put(entity.getPetName(), pestList);
        petsMap.put(entity.getPetId(), entity);
    }

    @Override
    public List<PetsTemplate> findPetByPetName(String entity) {
        return indexMap.get(entity);
    }

    @Override
    public void updatePetById(Integer value, String newPetName, Person newOwner, double petWeight) {
        petsMap.put(value, new PetsTemplate(newPetName, newOwner, petWeight, value));
    }

    @Override
    public List<PetsTemplate> printAllPets() {
        return new LinkedList<>(petsMap.values());
    }
}
