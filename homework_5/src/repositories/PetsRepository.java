package repositories;

import models.PetsTemplate;

public interface PetsRepository extends CrudRepository<PetsTemplate> {
}
