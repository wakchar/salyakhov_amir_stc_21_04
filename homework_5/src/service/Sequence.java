package service;

import java.util.concurrent.atomic.AtomicInteger;

public class Sequence {
    private static final AtomicInteger currentId = new AtomicInteger(10000);

    public static int nextId() {
        return currentId.incrementAndGet();
    }
}
