package models;

public class Person {

    private final String firstName;
    private final int personAge;
    private final String genderPerson;

    public Person(String firstName, int personAge, String genderPerson) {
        this.firstName = firstName;
        this.personAge = personAge;
        this.genderPerson = genderPerson;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", personAge=" + personAge +
                ", genderPerson='" + genderPerson + '\'' +
                '}';
    }
}
