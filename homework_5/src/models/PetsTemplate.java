package models;

import service.Sequence;

import java.util.Objects;

public class PetsTemplate {

    private final int petId;
    private final String petName;
    private final Person ownerOfPet;
    private final double petWeightInKg;

    public PetsTemplate(String petName, Person ownerOfPet, double petWeightInKg) {
        this.petName = petName;
        this.ownerOfPet = ownerOfPet;
        this.petWeightInKg = petWeightInKg;
        this.petId = Sequence.nextId();
    }

    public PetsTemplate(String petName, Person ownerOfPet, double petWeightInKg, int petId) {
        this.petName = petName;
        this.ownerOfPet = ownerOfPet;
        this.petWeightInKg = petWeightInKg;
        this.petId = petId;
    }

    public int getPetId() {
        return petId;
    }

    public String getPetName() {
        return petName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PetsTemplate that = (PetsTemplate) o;
        return Objects.equals(petName, that.petName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(petId, petName, ownerOfPet, petWeightInKg);
    }

    @Override
    public String toString() {
        return "PetsTemplate{" +
                "petId=" + petId +
                ", petName='" + petName + '\'' +
                ", ownerOfPet=" + ownerOfPet +
                ", petWeightInKg=" + petWeightInKg +
                '}';
    }
}
