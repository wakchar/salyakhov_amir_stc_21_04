import models.Person;
import models.PetsTemplate;
import repositories.PetsRepositoryImpl;

public class Main {

    public static void main(String[] args) {

        Person PersonMark = new Person("Mark", 18, "Male");
        Person PersonAnna = new Person("Anna", 12, "Female");

        PetsTemplate maoMao = new PetsTemplate("MaoMao", PersonMark, 2.2);
        PetsTemplate maoYao = new PetsTemplate("MaoYao", PersonMark, 1.4);
        PetsTemplate murzik = new PetsTemplate("Murzik", PersonMark, 1);
        PetsTemplate pushok = new PetsTemplate("Pushok", PersonAnna, 3.5);
        PetsTemplate pushokMark = new PetsTemplate("Pushok", PersonMark, 2.1);

        PetsRepositoryImpl petsRepository = new PetsRepositoryImpl();
        petsRepository.addPetToList(maoMao);
        petsRepository.addPetToList(murzik);
        petsRepository.addPetToList(pushok);
        petsRepository.addPetToList(pushokMark);

        System.out.println("-------------Search-------------");
        System.out.println(petsRepository.findPetByPetName("Pushok"));

        petsRepository.updatePetById(10004, "MaoWao", PersonAnna, 2.6);
        petsRepository.findPetByPetName("Pushok");
        System.out.println("-----------------------------");

        System.out.println(petsRepository.printAllPets());
        petsRepository.printAllPets();

        int end = 0;
    }
}
